using UnityEngine;

public class GuideObjectsController : MonoBehaviour
{
	[SerializeField] private Transform _spawnGiftGuidePoint;
	[SerializeField] private ObjectController _objectController;

	private GameObject CurrentGift;

	public void Init() => CurrentGift = _objectController.CreateGuideGift(_spawnGiftGuidePoint, this.transform).gameObject;
	public void DeleteCurrentGuideGift() => Destroy(CurrentGift);
}
