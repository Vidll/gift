using UnityEngine;

public abstract class LoadData : MonoBehaviour
{
    private void Awake()
	{
		SaveData.InitSaveData();
		EventSystemProject._takeValues += LoadValues;
		SaveData.Load();
	}
	
	protected virtual void LoadValues()
	{
		EventSystemProject._takeValues -= LoadValues;
	}
}
