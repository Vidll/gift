using System.IO;
using UnityEngine;

public static class SaveData 
{
    private static DataStruct _dataStruct = new DataStruct();

    private static string _filePath;
    private static string _fileName = "SavesSettings.json";

    public static bool GuideToSave { get { return _dataStruct.GuideToSave; } set { _dataStruct.GuideToSave = value; } }
    public static bool SoundToSave { get { return _dataStruct.SoundToSave; } set { _dataStruct.SoundToSave = value; } }

    public static void InitSaveData()
    {
#if PLATFORM_ANDROID && !UNITY_EDITOR
        _filePath = Path.Combine(Application.persistentDataPath, _fileName);
#else
        _filePath = Path.Combine(Application.dataPath, _fileName);
#endif

		if (!File.Exists(_filePath))
		{
            _dataStruct.GuideToSave = true;
            _dataStruct.SoundToSave = true;
            Save();
		}
    }

    public static void Save()
	{
        if (string.IsNullOrEmpty(_filePath))
            return;
        var json = JsonUtility.ToJson(_dataStruct, true);
        File.WriteAllText(_filePath, json);
	}

    public static void Load()
	{
        if (!File.Exists(_filePath))
		{
            Debug.LogError("Dont load file");
            return;
		}

        var json = File.ReadAllText(_filePath);
        _dataStruct = JsonUtility.FromJson<DataStruct>(json);
        EventSystemProject._takeValues.Invoke();
	}
}

[System.Serializable]
public struct DataStruct
{
    public bool GuideToSave;
    public bool SoundToSave;
}
