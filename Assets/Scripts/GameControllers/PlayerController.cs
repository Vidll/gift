using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	[SerializeField] private GameMechanics _gameMechanics;
	public GiftMechanics giftMechanics;

	private void Start()
	{
		_gameMechanics = Camera.main.gameObject.GetComponent<GameMechanics>();
	}

	public void GetRotationSlider(Slider slider)
	{
		if (giftMechanics == null)
			return;

		var value = GetValue(slider);
		value = GetPercent(value, 90f);

		if(!_gameMechanics.PauseState)
			giftMechanics.Rotate(value);
	}

	public void GetScaleSlider(Slider slider)
	{
		if (giftMechanics == null)
			return;

		var value = GetValue(slider);

		if (!_gameMechanics.PauseState)
			giftMechanics.ChangeScale(value);
	}

	private float GetPercent(float value, float maxValue) => value * maxValue;
	private float GetValue(Slider slider) => slider.value;
}