using System.Collections;
using UnityEngine;
using GooglePlayGames;

public class ScoreController : MonoBehaviour
{
	[SerializeField] private GameMechanics _gameMechanics;

	[SerializeField] private TextMesh _scoreOnScreen;
	[SerializeField] private TextMesh _scoreTextOnScreen;

	[SerializeField] private Color _sosoColor;

	[SerializeField] private int _score = 0;
	[SerializeField] private int _formerScoreDifficultChanger = 20;
	[SerializeField] private int _scorePoints = 10;

	private string _leaderBoardId = "CgkIlv7bxKcKEAIQAA";

	private void Start()
	{
		_gameMechanics = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameMechanics>();
		SetScoreNumber();
		SetScoreText("");

		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate();
		Social.localUser.Authenticate(success => { });
	}

	public void ResetScore()
	{
		_score = 0;
		_formerScoreDifficultChanger = 50;
		_scorePoints = 10;
		SetScoreNumber();
		SetScoreText("");
	}

	public void CalculateScore(float rotationRemains, float scaleRemains)
	{
		var mathRotationRemains = (int)Mathf.Round(rotationRemains);
		var mathScaleRemains = (int)Mathf.Round(scaleRemains);

		StartCoroutine(DeleteTextScoreOnTv());

		if ((mathRotationRemains <= 20 && mathScaleRemains <= 10) && //WARNING: Bad Code!!!
			(mathRotationRemains >= -20 && mathScaleRemains >= -10))
		{
			if ((mathRotationRemains <= 9 && mathScaleRemains <= 5) &&
				(mathRotationRemains >= -9 && mathScaleRemains >= -5))
			{
				if ((mathRotationRemains <= 4 && mathScaleRemains <= 2) &&
					(mathRotationRemains >= -4 && mathScaleRemains >= -2))
				{
					if ((mathRotationRemains <= 3 && mathScaleRemains <= 0) &&
						(mathRotationRemains >= -3 && mathScaleRemains >= 0))
					{
						SetColorOnText(Color.green);
						SetScore(-10, true);
						_gameMechanics.IncreaseLives();
						return;
					}
					SetColorOnText(Color.cyan);
					SetScore(0, true);
					return;
				}
				SetColorOnText(Color.blue);
				SetScore(5, true);
				return;
			}
			SetColorOnText(_sosoColor); 
			SetScore(7, true);
			return;
		}
		else
		{
			SetColorOnText(Color.red);
			_gameMechanics.SubtractLives();
			SetScore(_scorePoints + 5, true);
		}
	}

	private void SetScore(int value, bool view)
	{
		_score += _scorePoints - value;
		if (view)
		{
			if (_scorePoints - value > 0)
				SetScoreText("+" + (_scorePoints - value).ToString());
			else
				SetScoreText((_scorePoints - value).ToString());
		}
		
		Social.ReportScore(_score,_leaderBoardId, (bool success) => { });
		SetScoreNumber();

		if (_score >= _formerScoreDifficultChanger)
		{
			_gameMechanics.ChangeDifficult();
			_formerScoreDifficultChanger += 20;
		}
	}

	IEnumerator DeleteTextScoreOnTv()
	{
		yield return new WaitForSeconds(2.5f);
		SetScoreText("");
	}

	public void SetColorOnText(Color color) => _scoreTextOnScreen.color = color;
	public void SetScoreText(string text) => _scoreTextOnScreen.text = text;
	private void SetScoreNumber() => _scoreOnScreen.text = _score.ToString();
}