using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour
{
	private const string TAG_NAME_UI_OBJECTS = "UICanvas";

	[SerializeField] private GameMechanics _gameMechanics;
	[SerializeField] private PlayerController _playerController;
	[SerializeField] private ScoreController _scoreController;
    [SerializeField] private GiftMechanics _gift;
	[SerializeField] private GiftMechanics _currentGift;
	[SerializeField] private GameObject _form;

	private float _formRotation;
	private float _formScale;

	private void Start()
	{
		_gameMechanics = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameMechanics>();
		_scoreController = GameObject.FindGameObjectWithTag(TAG_NAME_UI_OBJECTS).GetComponent<ScoreController>();

		EventSystemProject._startGame += CreateGift;
	}

	public void CheckScore(float rotateValue)
	{
		var rotationRemains = _formRotation - rotateValue;
		var scaleRemains = (_formScale - _currentGift.transform.localScale.x) * 10;

		_scoreController.CalculateScore(rotationRemains, scaleRemains);
	}

	public void CreateGift()
	{
		if (!_gameMechanics.GetGameOver() && _gameMechanics.PlayState)
		{
			SetFormParameters();
			_currentGift = Instantiate(_gift);
			//_currentGift.GuideGift = false;
			_currentGift.transform.parent = transform;
			_playerController.giftMechanics = _currentGift;
		}
	}

	public GiftMechanics CreateGuideGift(Transform spawnPosition, Transform parent)
	{
		SetFormParameters();
		_currentGift = Instantiate(_gift, spawnPosition.position, Quaternion.identity);
		_currentGift.GuideGift = true;
		_currentGift.transform.parent = parent;
		_playerController.giftMechanics = _currentGift;
		return _currentGift;
	}

	private void SetFormParameters()
	{
		_formRotation = GetRandomNumber(0f, 90f);
		_form.transform.rotation = Quaternion.Euler(Vector3.up * _formRotation);

		_formScale = GetRandomNumber(0.2f, 0.5f); 
		_form.transform.localScale = new Vector3(_formScale, 0.5f , _formScale);
	}

	private float GetRandomNumber(float min, float max) => Random.Range(min, max);
}
