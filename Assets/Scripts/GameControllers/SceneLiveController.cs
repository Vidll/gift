using UnityEngine;

public class SceneLiveController : MonoBehaviour
{
    [SerializeField] private GameObject _liveObject1;
    [SerializeField] private GameObject _liveObject2;
    [SerializeField] private GameObject _liveObject3;


    public void SetLives(int number)
	{
		switch (number)
		{
			case 0:
				ActiveSceneLives(false, false, false);
				break;
			case 1:
				ActiveSceneLives(true, false, false);
				break;
			case 2:
				ActiveSceneLives(true, true, false);
				break;
			case 3:
				ActiveSceneLives(true, true, true);
				break;

			default:
				ActiveSceneLives(false, true, false);
				break;
		}
	}

	private void ActiveSceneLives(bool live1, bool live2,bool live3)
	{
		_liveObject1.SetActive(live1);
		_liveObject2.SetActive(live2);
		_liveObject3.SetActive(live3);
	}
}
