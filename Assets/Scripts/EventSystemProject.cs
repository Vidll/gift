public static class EventSystemProject
{
    public delegate void ClickOnButton();
    public delegate void ActionDelegate();
    public delegate void ClickOnButtonBool(bool value);

    public static ClickOnButton _clickOnPlay;
    public static ClickOnButtonBool _clickOnPause;
    public static ClickOnButton _clickOnSetting;
    public static ClickOnButton _clickOnInfo;
    public static ClickOnButton _showRewardVideo;

    public static ActionDelegate _startGame;
    public static ActionDelegate _pauseGame;
    public static ActionDelegate _gameOver;

    public static ActionDelegate _enablePlayButton;
    public static ActionDelegate _badResult;
    public static ActionDelegate _takeValues;
}
