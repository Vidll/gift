using UnityEngine;

public class GiftMechanics : MonoBehaviour
{
	[SerializeField] private ObjectController _objectController;
	[SerializeField] private bool _finish = false;

	private float _eulerRotate;
	private float _finishRotateValue;

	public bool IsMoved = false;
	public bool GuideGift = false;
	public float DestroyYPosition = -5f;

	private void Start()
	{
		_objectController = this.transform.parent.gameObject.GetComponent<ObjectController>();
	}

	private void Update()
	{
		if(transform.position.y < DestroyYPosition)
		{
			_objectController.CheckScore(_finishRotateValue);
			_objectController.CreateGift();
			DestroyGift();
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Finish"))
		{
			_finish = true;
			_finishRotateValue = transform.rotation.eulerAngles.y;
		}
	}

	public void Rotate(float value)
	{
		if (_finish)
			return;

		_eulerRotate = value;
		transform.rotation = Quaternion.Euler(Vector3.up * _eulerRotate);
	}

	public void ChangeScale(float value)
	{
		if (_finish)
			return;

		transform.localScale = new Vector3(value, transform.localScale.y, value);
	}

	private void DestroyGift() => Destroy(this.gameObject);
	public float GetEulerRotate() => _eulerRotate;
}
