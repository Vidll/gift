using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineBoxMechanics : MonoBehaviour
{
	[SerializeField] private GameMechanics _gameMechanics;
	[SerializeField] private float _moveSpeed = 30f;

	private GiftMechanics _giftMechanics;
	private bool _objectIsMoved = false;
	private float _currentSpeed;

	public float MoveSpeed { get {return _moveSpeed; } }

	private void Start()
	{
		_gameMechanics = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameMechanics>();
		_currentSpeed = _moveSpeed;
	}

	private void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.TryGetComponent(out GiftMechanics giftMechanics) && !_gameMechanics.GetGameOver())
		{
			_giftMechanics = giftMechanics;
			if (!_giftMechanics.IsMoved && !_giftMechanics.GuideGift)
			{
				_giftMechanics.IsMoved = true;
				_objectIsMoved = true;
			}
		}
	}

	private void OnCollisionStay(Collision collision)
	{
		if (_objectIsMoved && collision.gameObject.TryGetComponent(out Rigidbody rigidbody))
			rigidbody.velocity = Vector3.right * _currentSpeed * Time.deltaTime;
	}

	private void OnCollisionExit(Collision collision)
	{
		_objectIsMoved = false;
	}

	public void SetSpeed(float value)
	{
		_moveSpeed = value;
		_currentSpeed = _moveSpeed;
	}

	public void PauseEnable() => _currentSpeed = 0;
	public void PauseDisable() => _currentSpeed = _moveSpeed;
}
