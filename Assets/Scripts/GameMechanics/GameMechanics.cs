using UnityEngine;
using GooglePlayGames;

public class GameMechanics : MonoBehaviour
{
	private const string TAG_NAME_UI_CANVAS = "UICanvas";

	[SerializeField] private MachineBoxMechanics _machineBoxMechanics;
	[SerializeField] private SceneLiveController _sceneLiveController;
	[SerializeField] private ScoreController _scoreController;
	[SerializeField] private AdInitialize _adInitialize;
	[SerializeField] private Guide _guide;
	[SerializeField] private InformationWindow _informationWindow;

	[SerializeField] private int _lives = 3;
    [SerializeField] private bool _gameOver = false;

	private bool _pauseState = false;
	private bool _playState = false;

	public bool PauseState { get { return _pauseState; } set { _pauseState = value; } }
	public bool PlayState { get { return _playState; } set { _playState = value; } }
	
	private void Start()
	{
		if(_scoreController)
			_scoreController = GameObject.FindGameObjectWithTag(TAG_NAME_UI_CANVAS).GetComponent<ScoreController>();

		if (_adInitialize)
		{
			_adInitialize = FindObjectOfType<AdInitialize>();
			_adInitialize.RewardSuccessEvent += FullRewadContinueGame;
		}

		EventSystemProject._clickOnPlay += StartGame;
		EventSystemProject._clickOnPause += PauseGame;
		EventSystemProject._clickOnInfo += OpenInfo;
	}

	private void StartGame()
	{
		if (!_playState && !_guide.IsGuide)
		{
			GameOverDisabled();
			RestartGame();
			EventSystemProject._startGame.Invoke();
		}
	}

	private void RestartGame()
	{
		_scoreController.ResetScore();
		_machineBoxMechanics.SetSpeed(20f);
		_lives = 3;
		_sceneLiveController.SetLives(_lives);
	}

	private void RewardContinueGame()
	{
		_lives++;
		ContinueGameAndSetLives();
	}
	
	private void FullRewadContinueGame()
	{
		_lives = 3;
		ContinueGameAndSetLives();
	}

	private void ContinueGameAndSetLives()
	{
		_sceneLiveController.SetLives(_lives);
		GameOverDisabled();
		EventSystemProject._startGame.Invoke();
	}

	private void PauseGame(bool value)
	{
		if (value && _pauseState && _playState)
			return;
		else if (value && !_pauseState && _playState)
			PauseEnable();
		else if (!value && _pauseState && _playState)
			PauseDisable();
		else if (!value && !_pauseState && _playState)
			PauseEnable();
	}

	private void PauseEnable()
	{
		EventSystemProject._pauseGame.Invoke();
		_machineBoxMechanics.PauseEnable();
		_pauseState = true;
	}

	private void PauseDisable()
	{
		EventSystemProject._pauseGame.Invoke();
		_machineBoxMechanics.PauseDisable();
		_pauseState = false;
	}

	public void GameOverEnabled()
	{
		_gameOver = true;
		_playState = false;
		EventSystemProject._gameOver.Invoke(); // ��� ����������� �����������
	}

	public void GameOverDisabled()
	{
		_playState = true;
		_gameOver = false;
	}

	public void SubtractLives()
	{
		_lives--;
		_sceneLiveController.SetLives(_lives);
		EventSystemProject._badResult.Invoke();
		
		if(_lives == 0)
			GameOverEnabled();
	}

	public void IncreaseLives() 
	{
		if(_lives < 3)
		{
			_lives++;
			_sceneLiveController.SetLives(_lives);
		}
	}

	public void OnClickExitGame()
	{
		PlayGamesPlatform.Instance.SignOut();
		Application.Quit();
	}

	[ContextMenu("CreateScreenShot")]
	private void CreateScreenShot() => ScreenCapture.CaptureScreenshot("ScreenShot.png");
	public void OnClickShowLeaderBoard() => Social.ShowLeaderboardUI();
	public void ChangeDifficult() => _machineBoxMechanics.SetSpeed(_machineBoxMechanics.MoveSpeed + 2);
	private void OpenInfo() => _informationWindow.MoveUp();
	public bool GetGameOver() => _gameOver;
	public int GetLives() => _lives;
}
