using System.Collections;
using UnityEngine;

public class SoundManager : LoadData
{
	[SerializeField] private AudioClip[] _musics;
	[SerializeField] private AudioSource _audioSource;

	private int _musicIndex = 0;

	private bool _soundEnable = true;
	public bool SoundEnable { set { _soundEnable = value; } }

	private void Start()
	{
		EventSystemProject._clickOnSetting += SoundManaged;
	}

	private void SoundManaged()
	{
		if (_soundEnable)
			SoundOff();
		else
			SoundOn();
	}

	private void SoundCheckState()
	{
		if (_soundEnable)
			SoundOn();
		else
			SoundOff();
	}

	private void SoundOn()
	{
		_soundEnable = true;
		_musicIndex = Random.Range(0, _musics.Length);
		StartCoroutine(nameof(MusicPlaying));
	}

	private void SoundOff()
	{
		if (!_soundEnable)
			return;

		_soundEnable = false;
		_audioSource.Stop();
		StopCoroutine(nameof(MusicPlaying));
	}

	protected override void LoadValues()
	{
		base.LoadValues();
		_soundEnable = SaveData.SoundToSave;
		SoundCheckState();
	}

	private IEnumerator MusicPlaying()
	{
		while (_soundEnable)
		{
			_audioSource.clip = _musics[_musicIndex];
			_audioSource.Play();
			yield return new WaitForSeconds(_audioSource.clip.length);

			int newIndex = Random.Range(0, _musics.Length);
			while (_musicIndex == newIndex)
				newIndex = Random.Range(0, _musics.Length);

			_musicIndex = newIndex;
		}
	}
}
