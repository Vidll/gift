using System.Collections;
using UnityEngine;

public class InformationWindow : MonoBehaviour
{
	[SerializeField] private Vector3[] _points;
	[SerializeField] private float _speed;

	private bool _checkUp = false;
	private bool _checkDown = false;
	private IEnumerator Moved(Vector3 nextPos)
	{
		var available = true;
		while (available)
		{
			yield return null;
			transform.position = Vector3.Lerp(transform.position, nextPos, _speed * Time.deltaTime);
			if(_checkUp && transform.position.y > nextPos.y - 0.2f)
			{
				available = false;
				_checkUp = false;
			}
			else if (_checkDown && transform.position.y < nextPos.y + 0.2f)
			{
				available = false;
				_checkDown = false;
			}
		}
		yield return true;
	}

	public void MoveUp()
	{
		_checkUp = true;
		StartCoroutine(Moved(_points[0]));
	}

	public void MoveDown()
	{
		_checkDown = true;
		StartCoroutine(Moved(_points[1]));
	}
}
