using UnityEngine;

public class RewardWindow : MonoBehaviour
{
	[SerializeField] private float _speed;
	[SerializeField] private Vector3[] _points;

	private FillAmountAnimations3DObject _fillAmountAnimations3DObject;
	private Vector3 _nextPosition;
	private bool _moved = false;
	private bool _checkUp = false;
	private bool _checkDown = false;

	public Vector3 NextPosition { set { _nextPosition = value; } }
	public bool IsMoved { set { _moved = value; } }

	private void Start()
	{
		_fillAmountAnimations3DObject = GetComponentInChildren<FillAmountAnimations3DObject>();
	}

	private void Update()
	{
		if (!_moved)
			return;

		transform.position = Vector3.Lerp(transform.position, _nextPosition, _speed * Time.deltaTime);

		if(_checkUp && transform.position.y > _nextPosition.y - 0.2f)
		{
			IsMoved = false;
			_checkUp = false;
			_fillAmountAnimations3DObject.StartAnimation(MoveDown);
		}
		else if (_checkDown && transform.position.y < _nextPosition.y + 0.5f)
		{
			IsMoved = false;
			_checkDown = false;
		}
	}

	public void MoveUp()
	{
		_nextPosition = _points[0];
		IsMoved = true;
		_checkUp = true;
		_fillAmountAnimations3DObject.StartState();
	}

	public void MoveDown()
	{
		_nextPosition = _points[1];
		IsMoved = true;
		_checkDown = true;
	}
}
