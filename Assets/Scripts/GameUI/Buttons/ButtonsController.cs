using System.Collections;
using UnityEngine;

public class ButtonsController : LoadData
{
	[SerializeField] private Button[] _playAndRestartButtons;
	[SerializeField] private Button[] _soundButtons;
	[SerializeField] private float _waitAnimationTime = 0.5f;

	private bool _soundState = true;
	public bool SoundButtonState { set { _soundState = value; } }

	private void Start()
	{
		EventSystemProject._gameOver += EnableRestartButton;
		EventSystemProject._enablePlayButton += EnablePlayButton;
		EventSystemProject._clickOnSetting += StartSoundCorotine;

		_soundState = SaveData.SoundToSave;
	}

	private void EnableRestartButton()
	{
		_playAndRestartButtons[0].gameObject.SetActive(false);
		_playAndRestartButtons[1].gameObject.SetActive(true);

		EventSystemProject._gameOver -= EnableRestartButton;
		EventSystemProject._clickOnPlay += EnablePlayButton;
	}

	private void EnablePlayButton()
	{
		_playAndRestartButtons[0].gameObject.SetActive(true);
		_playAndRestartButtons[1].gameObject.SetActive(false);

		EventSystemProject._gameOver += EnableRestartButton;
		EventSystemProject._clickOnPlay -= EnablePlayButton;
	}

	private IEnumerator WaitAndChangeSoundButtons()
	{
		yield return new WaitForSeconds(_waitAnimationTime);
		ChangeSoundButtonState();
	}

	public void ChangeSoundButtonState()
	{
		if(_soundState)
		{
			_soundButtons[0].gameObject.SetActive(false);
			_soundButtons[1].gameObject.SetActive(true);
			_soundState = false;
		}
		else
		{
			_soundButtons[0].gameObject.SetActive(true);
			_soundButtons[1].gameObject.SetActive(false);
			_soundState = true;
		}

		SaveData.SoundToSave = _soundState;
		SaveData.Save();
	}

	public void SetSoundButtonState(bool value)
	{
		if (value)
		{
			_soundButtons[0].gameObject.SetActive(true);
			_soundButtons[1].gameObject.SetActive(false);
			_soundState = true;
		}
		else
		{
			_soundButtons[0].gameObject.SetActive(false);
			_soundButtons[1].gameObject.SetActive(true);
			_soundState = false;
		}
	}

	protected override void LoadValues()
	{
		base.LoadValues();
		_soundState = SaveData.SoundToSave;
		SetSoundButtonState(_soundState);
	}

	private void StartSoundCorotine() => StartCoroutine(WaitAndChangeSoundButtons());
}
