using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Button : MonoBehaviour
{
	private const string NAME_ANIMATOR_TRIGGER = "PressOnButton";
	private const string INFO_WEB_SITE = "https://play.google.com/store/apps/developer?id=Vidll+Games";

	private Animator _animator;
	public UnityEvent _currentEvent;

	private void Start()
	{
		_animator = GetComponent<Animator>();
	}

	private void OnMouseDown()
	{
		ButtonActive();
		ClickOnButton();
	}

	public void ShowRewardVideo()
	{
		EventSystemProject._showRewardVideo.Invoke();
	}

	public void PlayEvent()
	{
		StartCoroutine(WaitAndUse(EventSystemProject._clickOnPlay));
	}

	public void PauseEvent()
	{
		EventSystemProject._clickOnPause.Invoke(false);
	}

	public void SettingEvent()
	{
		StartCoroutine(WaitAndUse(EventSystemProject._clickOnSetting));
	}

	public void InfoEvent()
	{
		StartCoroutine(WaitAndUse(EventSystemProject._clickOnInfo));
		EventSystemProject._clickOnPause.Invoke(true);
	}

	private IEnumerator WaitAndUse(EventSystemProject.ClickOnButton customEvent )
	{
		yield return new WaitForSeconds(0.3f);
		customEvent.Invoke();
	}

	private void ClickOnButton()
	{
		if(_animator)
			_animator.SetTrigger(NAME_ANIMATOR_TRIGGER);
	}

	public void OpenURLGooglePlay() => Application.OpenURL(INFO_WEB_SITE);
	public void ButtonActive() => _currentEvent.Invoke();
}
