using UnityEngine;

public class Guide : LoadData
{
    [SerializeField] private GuideObjectsController _guideObjectsController;
	[SerializeField] private GameObject _guideUiObjects;
	[SerializeField] private GameObject _stage1;
	[SerializeField] private GameObject _stage2;
	[SerializeField] private UnityEngine.UI.Button _continueButton;

	private bool _guide = true;
	private int _currentStage = 0;
	public bool IsGuide { get { return _guide; } set { _guide = value; } }

	private void Start()
	{
		_continueButton.onClick.AddListener(OnClickContinueButton);

		EventSystemProject._clickOnPlay += DisableGuideEvent;
	}

	private void StartGuide()
	{
		if (!_guide)
			return;

		_guideUiObjects.SetActive(true);
		_stage1.SetActive(true);
		_stage2.SetActive(false);
		_currentStage = 1;
		_guideObjectsController.Init();
	}

	private void DisableGuideEvent()
	{
		_guideObjectsController.DeleteCurrentGuideGift();
		_guideUiObjects.SetActive(false);
		_guide = false;
		EventSystemProject._clickOnPlay -= DisableGuideEvent;
	}

	private void EnableStage2() 
	{
		_stage2.SetActive(true);
		_stage1.SetActive(false);
		_currentStage = 2;
	}

	private void OnClickContinueButton()
	{
		if (_currentStage == 1)
			EnableStage2();
		else if(_currentStage == 2)
		{
			_guide = false;
			SaveData.GuideToSave = _guide;
			SaveData.Save();
			EventSystemProject._clickOnPlay.Invoke();
		}
	}

	protected override void LoadValues()
	{
		base.LoadValues();
		_guide = SaveData.GuideToSave;

		if (_guide)
			StartGuide();
		else
		{
			EventSystemProject._clickOnPlay -= DisableGuideEvent;
			return;
		}
	}
}
