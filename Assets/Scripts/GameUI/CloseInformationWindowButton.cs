using UnityEngine;

public class CloseInformationWindowButton : MonoBehaviour
{
	[SerializeField] private InformationWindow _informationWindow;

	private void OnMouseDown()
	{
		_informationWindow.MoveDown();
	}
}
