using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    [Header ("UI Objects")]
    [SerializeField] private GameObject _pauseImage;
    [SerializeField] private GameObject _gameOverObject;
    [SerializeField] private GameObject _scoreObjet;
    [SerializeField] private RewardWindow _rewardWindow;

    [Header("Sertting")]
    [SerializeField] private AdInitialize _adInitialize;
    [SerializeField] private int _videoLives = 1;
    
    private bool _pauseImageState = false;
    private int _currentVideoLives;

    private void Start()
    {
        EventSystemProject._pauseGame += PauseImagStateChanger;
        EventSystemProject._gameOver += GameOverEnable;
        EventSystemProject._gameOver += RewardWindow;
        EventSystemProject._startGame += GameOverDisable;
        EventSystemProject._showRewardVideo += ShowVideo;
        _currentVideoLives = _videoLives;
    }

    [ContextMenu("OpenRewardWindow")]
    private void RewardWindow()
	{
        if(_currentVideoLives == 0 && _adInitialize.GetRewardedAdReady())
		{
            EventSystemProject._showRewardVideo.Invoke();
            _currentVideoLives = _videoLives;

		}
		else if (_adInitialize.GetRewardedAdReady())
		{
            _adInitialize.RewardSuccessEvent += CloseRewardWindow;
            _rewardWindow.MoveUp();
            _currentVideoLives--;
        }
        else
            _adInitialize.RewardLoad();
	}

    public void ShowVideo()
	{
        EventSystemProject._enablePlayButton.Invoke();
        _adInitialize.ShowRewad();
        _adInitialize.RewardLoad();
	}

    [ContextMenu("CloseRewardWindow")]
    private void CloseRewardWindow()
    {
        _adInitialize.RewardSuccessEvent -= CloseRewardWindow;
        _rewardWindow.MoveDown();
    }
    private void GameOverEnable()
    {
        _gameOverObject.SetActive(true);
        _scoreObjet.SetActive(false);
    }

    private void GameOverDisable()
    {
        _scoreObjet.SetActive(true);
        _gameOverObject.SetActive(false);
    }

	private void PauseImagStateChanger() => _pauseImage.SetActive(_pauseImageState = !_pauseImageState);
}
