using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdInitialize : MonoBehaviour
{
	[SerializeField] private bool _testMode;

	[SerializeField] private string _interstitialIdTest = "ca-app-pub-3940256099942544/1033173712";
	[SerializeField] private string _interstitialId = "ca-app-pub-8766262857163821/5907529384";

	[SerializeField] private string _rewardIdTest = "ca-app-pub-3940256099942544/5224354917";
	[SerializeField] private string _rewardId = "ca-app-pub-8766262857163821/5935449548";

	private InterstitialAd _interstitialAd;
	private RewardedAd _rewardedAd;

	public delegate void AdsEvent();
	public AdsEvent RewardSuccessEvent;

	private void Awake()
	{
		MobileAds.Initialize(initStatus => { });
		InterstitialLoad();
		RewardLoad();
	}

	[ContextMenu("ADS/InterstitialLoad")]
	public void InterstitialLoad()
	{
		_interstitialAd = new InterstitialAd( _testMode ? _interstitialIdTest : _interstitialId);
		AdRequest adRequest = new AdRequest.Builder().Build();
		_interstitialAd.LoadAd(adRequest);
	}

	[ContextMenu("ADS/Show Interstitial")]
	public void ShowInterstitialAd()
	{
		if (_interstitialAd.IsLoaded())
			_interstitialAd.Show();
		else
		{
			Debug.Log(name + ": interstitialAd don`t load");
			InterstitialLoad();
			ShowInterstitialAd();
		}
	}

	[ContextMenu("ADS/RewardLoad")]
	public void RewardLoad()
	{
		_rewardedAd = new RewardedAd(_testMode ? _rewardIdTest : _rewardId);
		AdRequest adRequest = new AdRequest.Builder().Build();
		_rewardedAd.LoadAd(adRequest);
		_rewardedAd.OnUserEarnedReward += SuccessReward;
	}

	[ContextMenu("ADS/ShowRewad")]
	public void ShowRewad()
	{
		if (_rewardedAd.IsLoaded())
			_rewardedAd.Show();
		else
		{
			Debug.Log(name + ": Reward don`t load");
			RewardLoad();
			ShowRewad();
		}
	}

	public bool GetInterstitialAdReady() => _interstitialAd.IsLoaded();
	public bool GetRewardedAdReady() => _rewardedAd.IsLoaded();
	private void SuccessReward(object sender, Reward e) => RewardSuccessEvent.Invoke();
}