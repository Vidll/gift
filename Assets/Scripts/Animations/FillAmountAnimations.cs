using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FillAmountAnimations : MonoBehaviour
{
	[SerializeField] private float _speedAnimation = 0.5f;

    private Image _image;
	private float _value;
	private bool _available;

	private Action _action;

	private void Start()
	{
		_image = GetComponent<Image>();
	}

	private void Update()
	{
		if (!_available)
			return;

		Animation();
	}

	public void Animation()
	{
		_image.fillAmount = Mathf.MoveTowards(_image.fillAmount, _value, _speedAnimation * Time.deltaTime);
		if (_image.fillAmount == _value)
			_action.Invoke();
	}

	public void StartAnimations(float EndValue, Action action)
	{
		_value = EndValue;
		_action = action;
		_available = true;
	}

	public void SetCurrentValue(float value)
	{
		if (_image)
			_image.fillAmount = value;
		else
			GetComponent<Image>().fillAmount = value;
	}
	public void StopAnimations() => _available = false;
}
