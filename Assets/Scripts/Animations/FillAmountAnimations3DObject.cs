using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FillAmountAnimations3DObject : MonoBehaviour
{
	[SerializeField] private Vector3 _startState;
	[SerializeField] private float _speed = 20f;

	private Vector3 _endState;
    private Transform _thisTransform;
	private bool _isMoved = false;

	private Action _currentEvent;

	private void Start()
	{
		_thisTransform = GetComponent<Transform>();
		_endState = new Vector3(_startState.x, 0f, _startState.z);
	}

	private void Update()
	{
		if (!_isMoved)
			return;

		_thisTransform.localScale = Vector3.MoveTowards(_thisTransform.localScale, _endState, _speed * Time.deltaTime);

		if (_thisTransform.localScale.y < _endState.y + 0.5f)
		{
			_isMoved = false;
			_currentEvent?.Invoke();
		}
	}

	public void StartAnimation(Action action)
	{
		_isMoved = true;
		_currentEvent = action;
	}

	public void StartState() => _thisTransform.localScale = _startState;
}
