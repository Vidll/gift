using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailLight : MonoBehaviour
{
	[SerializeField] private GameObject _ligth;

	//private EventSystemProject _eventSystemProject;

	private void Start()
	{
		//_eventSystemProject = FindObjectOfType<EventSystemProject>();
		EventSystemProject._badResult += StartCorotineLight;
	}

	IEnumerator BlinkLigth()
	{
		_ligth.SetActive(true);
		yield return new WaitForSeconds(0.4f);
		_ligth.SetActive(false);
		yield return new WaitForSeconds(0.3f);
		_ligth.SetActive(true);
		yield return new WaitForSeconds(0.3f);
		_ligth.SetActive(false);
	}

	private void StartCorotineLight() => StartCoroutine(BlinkLigth());
}
